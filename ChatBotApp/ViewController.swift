//
//  ViewController.swift
//  RecastAI
//
//  Created by plieb on 03/29/2016.
//  Copyright (c) 2017 plieb. All rights reserved.
//

import UIKit
import RecastAI
import Intents
import MBProgressHUD
/**
Class ViewController Example of implementations for Text & Voice Requests
 */
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate
{
    
    @IBOutlet weak var const_bottombtn: NSLayoutConstraint!
    
    @IBOutlet weak var const_bottom: NSLayoutConstraint!
    @IBOutlet weak var tbl_chat: UITableView!
    
    
    //Outlets
    @IBOutlet weak var textViewChat: UITextView!
    @IBOutlet weak var requestTextField: UITextField!
    var chatArray = NSMutableArray()
    //Vars
    var bot : RecastAIClient?
    var chatBool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.bot = RecastAIClient(token :"d7e9b7bfe1abef6f2e7ac43e3ffaf27c",language:"en")
        self.title = "Salon Chat Bot"
//        self.bot = RecastAIClient(token : "YOUR_TOKEN", language: "en")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    @objc func keyboardWillShow(sender: NSNotification) {
       // self.view.frame.origin.y = -150 // Move view 150 points upward
        self.const_bottom.constant = 255
        self.const_bottombtn.constant = 255

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X")
                self.const_bottom.constant = 300
                self.const_bottombtn.constant = 300

            default:
                print("unknown")
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
       // self.view.frame.origin.y = 0 // Move view to original position
        self.const_bottom.constant = 0
        self.const_bottombtn.constant = 0

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            self.tbl_chat.reloadData()
            
        }
        
        let dict = ["message":"Welcome to Salon Cloud Plus how can i help you.",
                    "type":"user"]
        self.chatArray.add(dict)
        self.textViewChat.layer.cornerRadius = 8
        self.textViewChat.layer.borderWidth = 1
        self.textViewChat.layer.borderColor = UIColor.gray.cgColor
        self.textViewChat.keyboardType = .default
    }
    
    
    /**
     Method called when the request was successful
     
     - parameter response: the response returned from the Recast API
     
     - returns: void
     */
    func recastRequestDone(_ response : Response)
    {
        print(response.timestamp)
        print(response.status)
        print(response.intents as Any)
        print(response.language)
        print(response.source)
    }
    
    /**
     Method called when the converse request was successful
     
     - parameter response: the response returned from the Recast API
     
     - returns: void
     */
    func recastRequestDone(_ response : ConverseResponse)
    {
        print(response.replies as Any)
       // print(response.action?. as Any)
    }
    
    /**
     Method called when the request failed
     
     - parameter error: error returned from the Recast API
     
     - returns: void
     */
    func recastRequestError(_ error : Error)
    {
        print("Error : \(error)")
    }
    
    /**
     Make text request to Recast.AI API
     */
    @IBAction func makeRequest()
    {
        if (!(self.requestTextField.text?.isEmpty)!)
        {
            let url = URL(string: self.requestTextField.text!)!
            //Call makeRequest with string parameter to make a text request
            // self.bot?.analyseFile(url, successHandler: recastRequestDone, failureHandle: recastRequestError)
//            self.bot?.textRequest(self.requestTextField.text!, successHandler: { (response) in
//                print(response)
//
//            }, failureHandle: { (error) in
//
//            })    }
        }
        
    }
    
    
    @IBAction func sendChat(_ sender: Any) {
        let dict = ["message":textViewChat.text!,
                    "type":"customer"]
       self.chatArray.add(dict)
        self.chatBool = true
     //   progress.show
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        if (!(self.textViewChat.text?.isEmpty)!)
        {
           // let url = URL(string: self.requestTextField.text!)!
//            Call makeRequest with string parameter to make a text request
//             self.bot?.analyseFile(url, successHandler: recastRequestDone, failureHandle: recastRequestError)
//
//
//            self.bot?.textConverse(self.textViewChat.text!, successHandler: { (core) in
//                print("response%@",core)
//            }, failureHandle: { (error) in
//
//            })
            
//            self.bot?.dialogText(textViewChat.text!, conversationId: "\(number)", successHandler: { (response) in
//                print("hello")
//            }, failureHandle: { (error) in
//                print("error")
//            })
//            self.bot?.textRequest(self.textViewChat.text!, successHandler: { (response) in
//                 self.chatBool = false
//                self.textViewChat.text = ""
//                print(response)
            
//              //   progress.show(animated: false)
//               // print("String%@",response)
//
//               let arr = response.intents
//                //print("response%@",arr![0])
//                //let dict = arr?.first
//               // let str = dict!["description"]
//               // self.chatArray.add(arr![0])
//
//            }, failureHandle: { (error) in
//
//            })
            let number = Int(arc4random_uniform(42))
            self.bot?.dialogText(textViewChat.text, conversationId: "\(number)", successHandler: { (dilog) in
                //
                //var message = Message()
                let message = dilog.messages
                for obj in message!{
                    let dict = ["message":obj.content as! String,
                                "type":"user"]
                    self.chatArray.add(dict)
                    DispatchQueue.main.async {
                        self.tbl_chat.reloadData()
                        
                    }
                }
               // print("Message : %@",message.content)
                MBProgressHUD.hide(for: self.view, animated: true)
                print("Dilog%@",dilog.messages)
                print(dilog.messages)
                self.textViewChat.text = ""
                //print(dilog.messages[0])
            }, failureHandle: { (error) in
                print("eroor%@",error)
            })
//            self.bot?.analyseText(textViewChat.text!, successHandler: { (response) in
//                print(response)
//            }, failureHandle: { (error) in
//                print(error)
//            })
            //self.tbl_chat.reloadData()
        }
       
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    /**
     Make File request to Recast.AI API
     */
    func makeFileRequest()
    {
        if (!(self.requestTextField.text?.isEmpty)!)
        {
            let url = URL(string: self.requestTextField.text!)!
            //Call makeRequest with string parameter to make a text request
           // self.bot?.analyseFile(url, successHandler: recastRequestDone, failureHandle: recastRequestError)
//            self.bot?.textRequest(self.requestTextField.text!, successHandler: { (response) in
//                print(response)
//
//
//            }, failureHandle: { (error) in
//
//            })
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = chatArray[indexPath.row] as? NSDictionary ?? [:]
        if dict["type"] as! String == "user"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatBotCell") as! ChatBotCell
            cell.lbl_bot.text = dict["message"] as? String ?? ""
            cell.lbl_bot.layer.borderColor = UIColor.lightGray.cgColor
            cell.lbl_bot.layer.cornerRadius = 20
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatUserCell") as! ChatUserCell
            cell.lbl_user.text = dict["message"] as? String ?? ""
            cell.lbl_user.layer.borderColor = UIColor.lightGray.cgColor
             cell.lbl_user.layer.cornerRadius = 20
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
